#!/bin/bash
source ./ezpublish.env.properties-good
VARIABLES=`set | grep -f ./ezpublish.env.properties-good | awk -F "=" '{print $1}'`
#Suppression des lignes qui suivent #DEV
find -name *.ini.append.php -print0 | xargs -0 sed -i '/#DEV/,+1d'
#Mise en place des settings avec le suffice __ENV__ :
find -name *.ini.append.php -print0 | xargs -0 sed -i 's/__ENV__//g'
#Suppression des autres environnements
find -name *.ini.append.php -print0 | xargs -0 sed -i '/__.*__/d'
#Applications des valeurs du fichier ezpublish.env.properties-good
for i in $VARIABLES;
do
VALUE=${!i}
PATTERN=@$i@
echo "s/$PATTERN/$VALUE/g"
find -name *.ini.append.php -print0 | xargs -0 sed -i "s!$PATTERN!$VALUE!g"
done


