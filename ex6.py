
# This line set x variable with the txt
# and %d is replace by the number 10 at the end 
x = "There are %d types of people. " % 10

# This line set variable binary = binary and
#do_not to don't

binary = "binary"
do_not = "don't"

#This line set the variable y contains the line and replace %s with binary 
#and %s with what's in the variable do_not.


y ="Those who know %s and those who %s" % (binary, do_not)



# this line print variable x and variable y.

print x ,"\n", y

# this line print the sring and %r is replace by whay in the variable x
print "I said : %r." %x
print "I said : '%s'." %y

# This line set variable hilarious to False 

hilarious = False
# THis line set Variable joke_evaluation
joke_evaluation = "Isn't that joke so funny ?! %r"

#this replace %r in joke_evalution with what's in hilarious.

print joke_evaluation % hilarious

# This  set variable w
w ="This is the left side of ..."

# this set variable e

e = " a string a right side."

# this print w e concatenate together on one line
print w + e

