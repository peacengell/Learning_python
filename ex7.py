print "Mary had a little lamb."
#this print Mary ...


print "Its fleece was white as %s." % 'snow'
# this print its fleece and replace %s with snow when it's get printed.

# this prints and the sentence.
print "And everywhere that Mary went."

# this print ten time "." 

print"." * 10 # what'd that do ?

# this set variable endx 
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

#Watch that comma at the end. try removing it to see what happens
# this print varable endx on one line
# if you remove the comma at end6, it print end 1 to end6 on one line
# and end 7 to end 12 on the seond line.

print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 + end11 + end12