# f1 = open('index_uxfrlb-hapweb1p_.txt', 'r')
# f2 = open('index_uxfrlb-hapweb1s_.txt', 'r')

# fileOne = f1.readlines()
# fileTwo = f2.readlines()

# f1.close()
# f2.close()

# #print fileOne + fileTwo

import difflib, sys

with open('test2.txt', 'r') as F1, open('test1.txt', 'r') as F2:
	diff = difflib.ndiff(F2.readlines(), F1.readlines())
	for line in diff:
		if line.startswith('-'):
			sys.stdout.write(line)
		elif line.startswith('+'):
			sys.stdout.write('\t\t'+line+"\n")