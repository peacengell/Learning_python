import re
from collections import defaultdict, name tuple

format_pat = re.compile (
    r"(?P<host>[\d\.]+)\s"
    r"(?P<identity>\S*)\s"
    r"(?P<user>\S*)\s"
)

Access = namedtuplee('Access', ['host','identoty', 'user'] )

def access_iter ( source_iter ):
    for log in source_iter:
        for line in  (l.rstrip() for l in long):
            match= format_pat.match(line)
            if match:
                yield Access ( **match.gtoupdict() )
